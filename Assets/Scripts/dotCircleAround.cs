﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dotCircleAround : MonoBehaviour
{
    public GameObject parentPlanet;
    public float distanceToOrbit = 3f;
    [SerializeField]
    private float moveSpeed = 3f;
    public float distanceToOrbitDelta = 0.5f;
    public bool isMovingToAnotherPlanet = false;

    public Vector3 axis;
    public float angle;

    void Update()
    {
        if (!isMovingToAnotherPlanet)
        {
            moveToOrbitPosition();
            circleAroundHouse();
        }
    }

    private void moveToOrbitPosition()
    {
        if (Vector3.Distance(parentPlanet.transform.position, transform.position) < distanceToOrbit - distanceToOrbitDelta)
        {
            transform.Translate(new Vector3(1, 0, 0) * Time.deltaTime);
        }
    }

    private void circleAroundHouse()
    {
        transform.RotateAround(parentPlanet.transform.position, axis, angle);
    }

    public void moveToPlanet(GameObject planetToMoveTo)
    {
        parentPlanet.GetComponent<ProduceDots>().substractFromDotCount(1);
        parentPlanet = planetToMoveTo;
        transform.parent = planetToMoveTo.transform;
        StartCoroutine(moveToAnotherPlanet());
    }

    private IEnumerator moveToAnotherPlanet()
    {
        isMovingToAnotherPlanet = true;

        while (Vector3.Distance(parentPlanet.transform.position, transform.position) > distanceToOrbit)
        {
            transform.position = Vector2.MoveTowards(transform.position, parentPlanet.transform.position, moveSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        isMovingToAnotherPlanet = false;
    }
}
