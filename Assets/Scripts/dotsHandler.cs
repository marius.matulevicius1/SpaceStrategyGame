﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dotsHandler : MonoBehaviour
{
    private static dotsHandler _instance;
    public static dotsHandler Instanse {get{return _instance;}}
    public List<GameObject> allDots = new List<GameObject>();
    private void Awake() {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }

    public List<GameObject> getAllDotsOnSpecifiedPlanet(GameObject planet){
        List<GameObject> result = new List<GameObject>();

        for (int i = 0; i < result.Count; i++)
        {
            if (allDots[i].GetComponent<dotCircleAround>().parentPlanet == planet)
            {
                result.Add(allDots[i]);
            }
        }
       

        return result;
    }
}
