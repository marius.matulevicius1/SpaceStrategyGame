﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePlanetController : MonoBehaviour
{

    public int maximumAmountOfDotsOnPlanetToProduce = 30;

    public TextMesh Text;

    ProduceDots produceDots;
    void Start()
    {
        Text = gameObject.GetComponentInChildren<TextMesh>();
        produceDots = GetComponent<ProduceDots>();
    }

    void Update()
    {
            produceDots.produceDot();
    }
}
