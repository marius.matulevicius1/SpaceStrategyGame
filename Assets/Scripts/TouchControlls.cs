﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControlls : MonoBehaviour
{
    private LineRenderer lineRenderer;
    public GameObject startMoveObject;
    public GameObject endMoveObject;

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }
    // Update is called once per frame
    void Update()
    {
        touchControlls();
        alternativeMouseControlls();
    }

    private void touchControlls()
    {
        if (Input.touchCount > 0)
        {
            Vector2 touchPosition = Input.GetTouch(0).position;
            if (Input.GetTouch(0).phase == TouchPhase.Began) onTouchBegan(touchPosition);
            if (Input.GetTouch(0).phase == TouchPhase.Moved) onTouchMoved(touchPosition);
            if (Input.GetTouch(0).phase == TouchPhase.Ended) onTouchEnded(touchPosition);
        }


    }

    private void onTouchBegan(Vector2 touchPosition)
    {
        Ray raycast = Camera.main.ScreenPointToRay(touchPosition);
        RaycastHit raycastHit;
        if (Physics.Raycast(raycast, out raycastHit))
        {
            if (raycastHit.collider.CompareTag("Planet"))
            {
                lineRenderer.enabled = true;
                Vector2 screenToWorld = Camera.main.ScreenToWorldPoint(touchPosition);
                Vector3 position = new Vector3(screenToWorld.x, screenToWorld.y, 0);
                lineRenderer.SetPosition(0, raycastHit.transform.position);
                lineRenderer.SetPosition(1, position);
                startMoveObject = raycastHit.collider.gameObject;
            }
        }
    }

    private void onTouchMoved(Vector2 touchPosition)
    {
        Vector2 screenToWorld = Camera.main.ScreenToWorldPoint(touchPosition);
        Vector3 position = new Vector3(screenToWorld.x, screenToWorld.y, 0);
        lineRenderer.SetPosition(1, position);
    }

    private void onTouchEnded(Vector2 touchPosition)
    {
        lineRenderer.enabled = false;
        Ray raycast = Camera.main.ScreenPointToRay(touchPosition);
        RaycastHit raycastHit;
        if (Physics.Raycast(raycast, out raycastHit))
        {
            if (raycastHit.collider.CompareTag("Planet"))
            {
                dotsHandler handler = GetComponent<dotsHandler>();
                endMoveObject = raycastHit.collider.gameObject;
                dotCircleAround[] dots = startMoveObject.GetComponentsInChildren<dotCircleAround>();

                foreach (var item in dots)
                {
                    item.moveToPlanet(endMoveObject);
                }
            }
        }


    }

    private void alternativeMouseControlls()
    {
        if (Input.GetMouseButtonDown(0))
        {
            onTouchBegan(Input.mousePosition);
        }
        if (Input.GetMouseButton(0))
        {
            onTouchMoved(Input.mousePosition);
        }
        if (Input.GetMouseButtonUp(0))
        {
            onTouchEnded(Input.mousePosition);
        }
    }
}
