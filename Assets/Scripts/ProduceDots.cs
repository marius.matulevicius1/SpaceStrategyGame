﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProduceDots : MonoBehaviour
{
    public float amountToProducePerSecond = 1;
    GameObject dotPrefab;
    public int maximumAmoutOfDots = 30;
    public int dotCount = 0;
    TMPro.TextMeshProUGUI amountTextObject;



    float time = 0f;
    void Start()
    {
        dotPrefab = Resources.Load("glowingDot") as GameObject;
        calculateAmountOfDotsOnPlanet();
        amountTextObject = transform.GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>();
    }

    public void produceDot()
    {
        if (dotCount >= maximumAmoutOfDots) return;

        time += Time.deltaTime;
        if (time >= 1)
        {
            GameObject dot = Instantiate(dotPrefab);
            dot.transform.position = transform.position;
            dot.GetComponent<dotCircleAround>().parentPlanet = gameObject;
            dot.GetComponent<dotCircleAround>().distanceToOrbit = 3f;
            dot.transform.parent = gameObject.transform;
            time = 0f;
            addToDotCount(1);
            dotsHandler.Instanse.allDots.Add(dot);
        }
    }

    public void calculateAmountOfDotsOnPlanet()
    {
        GameObject[] dots = GameObject.FindGameObjectsWithTag("dot");

        for (int i = 0; i < dots.Length; i++)
        {
            if (dots[0].GetComponent<dotCircleAround>().parentPlanet == gameObject)
            {
                dotCount++;
            }
        }
    }

    public void addToDotCount(int valueToAdd)
    {
        dotCount += valueToAdd;
        amountTextObject.text = dotCount.ToString();
    }

    public void substractFromDotCount(int valueToSubstract)
    {
        dotCount -= valueToSubstract;
        amountTextObject.text = dotCount.ToString();
    }
}
